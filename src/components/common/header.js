import React from 'react';
import logo from '../../asset/images/logo192.png'
import { Menu } from 'antd';

function AppHeader() {
    return (
        <div className='container-fluid'>
            <div className="header justify-content-between">
                <div className="logo">
                    
                    <img height={50} src={logo} alt='logo'/>
                    <a href="google.com">Company</a>
                    
                </div>
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['home']}>
                    <Menu.Item key="home">home</Menu.Item>
                    <Menu.Item key="about">about</Menu.Item>
                    <Menu.Item key="features">features</Menu.Item>
                    <Menu.Item key="how it works">how it works</Menu.Item>
                    <Menu.Item key="faq">faq</Menu.Item>
                    <Menu.Item key="pricing">pricing</Menu.Item>
                    <Menu.Item key="contact">contact</Menu.Item>
                </Menu>
            </div>

        </div>
    );
}
export default AppHeader;

