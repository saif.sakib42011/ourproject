import React from 'react';
import { Carousel } from 'antd';
const items = [
    {
        key: '1',
        title: 'Web Development',
        content: 'SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE'
    },

    {
        key: '2',
        title: 'Web DeSIGN',
        content: 'SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE'
    },

    {
        key: '3',
        title: 'HACKING',
        content: 'SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE SOME TEXT GO HERE'
    }

];

function AppHero() {
    return (
        <div className='heroBlock'>
                <Carousel autoplay>
                    {items.map(item => {
                        return(
                            <div className='content'>
                                <h2 contentStyle>{item.title}</h2>
                                <p contentStyle>{item.content}</p>    
                            </div>
                        );
                    })}
                </Carousel>
        </div>            
        
    
    );
}
export default AppHero;
